<?php
require __DIR__ . "/../config.php";


require PHP_DIR . "class/Autoloader.php";
Autoloader::register();

use magic\Template;

?>

<?php ob_start() ?>

<header style="margin: 15px; text-align: center">
    <h1>Renaud Ducrocq</h1>
    <div>Etudiant en Licence Informatique</div>
</header>
<article>
    <h2>
        Formation
    </h2>
    <p>
    <ul>
        <li>2020-2022: Licence Informatique, Université de Calais</li>
        <li>2020: Diplôme Universitaire Technologique (DUT) en Génie Electrique et Informatique Industrielle a l'IUT
            côte d'opale, site de Calais
        </li>
        <li>2018: Baccalauréat scientifique au Lycée Mariette de Boulogne-sur-Mer, section Européenne spécialité
            mathématiques avec mention
        </li>
    </ul>
    </p>
</article>
<article>
    <h2>
        Compétences
    </h2>
    <p>
    <ul>
        <li>
            Langages informatiques : C/C++, JAVA, Python, PHP, Javascript, C#, HTML/CSS, SQL
        </li>
        <li>
            Principes de développement : Programmation orientée objet, Gestion de bases de données, développement web
            statique/dynamique, Intelligence artificielle
        </li>
    </ul>
    </p>
</article>
<article>
    <h2>
        Expériences Professionnelles
    </h2>
    <p>
    <ul>
        <li>
            Actuellement en CDI Etudiant chez LIDL depuis plus d'un an, je suis equipier polyvalent. Je m'occupe aussi
            bien de la caisse que des rayons et du pain.
        </li>
        <li>
            J'ai aussi effectué des missions en tant qu'animateur en centre de loisirs, et en séjour de vacances pour
            UCPA ou SNCF.
        </li>
    </ul>
    </p>
</article>
<article>
    <h2>
        Centres d'intérêts
    </h2>
    <p>
    <div>
        Tous domaines permettant de développer ma culture
    </div>
    <ul>
        <li>
            Lecture : revues scientifiques et informatiques, romans
        </li>
        <li>Sport : escalade, volley, catamaran, badminton
        </li>
        <li>Cinéma</li>
    </ul>
    </p>
</article>

<?php $code = ob_get_clean() ?>
<?php Template::render($code); ?>

