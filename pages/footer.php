<footer class="bg-dark text-center text-white">
    <div class="container">
        <div class="row">

            <div class="container p-4 pb-0">
                <h4>Renaud Ducrocq</h4>
                <ul class="list-unstyled">
                    <li>
                        <a class="text-light text-decoration-none" href="<?php echo DOCUMENT_DIR ?>pages/CV.php">
                            CV
                        </a>
                    </li>
                    <li>
                        <a class="text-light text-decoration-none" href="<?php echo DOCUMENT_DIR ?>pages/projets.php">
                            Portfolio
                        </a>
                    </li>
                    <li>
                        <a class="text-light text-decoration-none" href="<?php echo DOCUMENT_DIR ?>pages/reseaux.php">
                            Reseaux
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</footer>