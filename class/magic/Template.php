<?php


namespace magic;


class Template
{

    public static function render($code) : void{ ?>

        <!doctype html>
        <html lang="fr">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport"
                  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Renaud Ducrocq</title>

            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

            <link rel="stylesheet" href="/css/main-style.css">

        </head>
        <body>
        <?php include PHP_DIR."pages/header.php" ; ?>
        <div id="main-content" class="item">

            <?php echo $code ?>

        </div> <!-- #main-content -->
        <?php include PHP_DIR."pages/footer.php" ?>
        </body>
        </html
    <?php
    }

}