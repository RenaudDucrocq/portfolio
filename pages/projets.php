<?php
require __DIR__ . "/../config.php";

session_start();

require PHP_DIR . "class/Autoloader.php";
Autoloader::register();

use magic\Template;

?>

<?php ob_start() ?>
<div class="row gy-4" style="margin-top: 1.5em; margin-bottom: 1.5em">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Learning Souls Game</h5>
                <hr>
                <h6 class="card-subtitle mb-2">Description</h6>
                <p class="card-text">Ce projet a pour but la réalisation d'un mini jeu avec JAVA en utilisant
                    l'interface graphique JAVAFX.
                    C'est un projet scolaire réalisé en coordination avec le Professeur sous forme d'étapes afin de nous
                    familiariser avec le langage orienté objet, et avec les outils graphiques JAVAFX. Le but final n'est
                    donc pas
                    d'avoir un jeu complet ni optimisé mais de prendre en main différents outils de développement.</p>
                <h6 class="card-subtitle mb-2">Ressources</h6>
                <p class="card-text">Merci à Gregory Bourguin Docteur en informatique, Maître de conférence LISIC,
                    Enseignant à l'ULCO.</p>
                <hr>
                <h6 class="card-subtitle mb-2">Aperçu</h6>
                <img src="../images/image-1.png" class="card-img" alt="Card image cap">
                <hr>
                <h6 class="card-subtitle mb-2">Sources</h6>
                <a href="http://web.gregory-bourguin.fr/" class="card-link">Le site de Gregory Bourguin</a><br>
                <a href="https://gitlab.com/RenaudDucrocq/mini_jeu_java" class="card-link">Le dêpot Gitlab</a>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">2DUnity</h5>
                <hr>
                <h6 class="card-subtitle mb-2">Description</h6>
                <p class="card-text">Ce projet est une initiative personnelle ayant pour but la prise en main de la
                    suite Unity.
                    Il est basé sur les tutoriels de la chaîne <a
                            href="https://www.youtube.com/channel/UCJRwb5W4ZzG43J5_dViL6Fw/videos">TUTO UNITY FR</a>. Le
                    but final n'est
                    donc pas d'avoir un jeu complet ni optimisé mais de prendre en main différents outils de
                    développement.</p>
                <h6 class="card-subtitle mb-2">Ressources</h6>
                <p class="card-text">Merci à la chaîne TUTO UNITY FR qui fournit des tutos videos complet ainsi que les
                    codes sources si nécessaire</p>
                <hr>
                <h6 class="card-subtitle mb-2">Aperçu</h6>
                <img src="../images/2DUnity.png" class="card-img" alt="Card image cap">
                <hr>
                <h6 class="card-subtitle mb-2">Sources</h6>
                <a href="https://www.tutounity.fr/" class="card-link">Le site de la chaîne</a><br>
                <a href="https://gitlab.com/RenaudDucrocq/projet_personnel_unity_2d" class="card-link">Le dêpot
                    Gitlab</a>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">LEGO Store</h5>
                <hr>
                <h6 class="card-subtitle mb-2">Description</h6>
                <p class="card-text">Ce Projet a pour but le développement d'une application simplifiée de e-commerce
                    pour une boutique de LEGO. Il est réalisé dans un cadre scolaire en coordination avec le professeur
                    sous forme d'étapes. L'application sera développée avec PHP, MariaDB et adoptera une
                    architecture Modèle-Vue-Contrôleur. </p>
                <h6 class="card-subtitle mb-2">Ressources</h6>
                <p class="card-text">Merci à Florian Lepretre Docteur en informatique, Enseignant à l'ULCO.</p>
                <hr>
                <h6 class="card-subtitle mb-2">Aperçu</h6>
                <video src="../images/cart.mp4" controls ></video>
                <hr>
                <h6 class="card-subtitle mb-2">Sources</h6>
                <a href="https://florian-lepretre.herokuapp.com/" class="card-link">Le site de Florian Lepretre</a><br>
                <a href="https://gitlab.com/RenaudDucrocq/ulco-l2-projetweb-etudiant" class="card-link">Le dêpot
                    Gitlab</a>
            </div>
        </div>
    </div>
</div>
<?php $code = ob_get_clean() ?>
<?php Template::render($code); ?>
