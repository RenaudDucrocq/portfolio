<?php
$logged = isset($_SESSION['nickname']);
?>
<div class="container-fluid">
    <div class="item">
        <header class="bg-dark">
            <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
                <!-- Brand -->
                <a class="navbar-brand" href="../index.php">Accueil</a>

                <!-- Toggler/collapsibe Button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Navbar links -->
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo DOCUMENT_DIR ?>pages/CV.php">CV</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo DOCUMENT_DIR ?>pages/projets.php">Portfolio</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        </header>
    </div>
