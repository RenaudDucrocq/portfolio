<?php
require_once "config.php";


require "class/Autoloader.php";
Autoloader::register();

use magic\Template;

?>

<?php ob_start() ?>
    <div class="title">
        <h1>Je m'appelle Renaud Ducrocq</h1>
        <h2>Je suis étudiant en informatique</h2>
        <h3>Venez me découvrir ! </h3>
    </div>

<?php $code = ob_get_clean() ?>
<?php Template::render($code);
